namespace DomainModel
{
    public class OrderItem
    {
        public int Id { get; protected set; }

        public int OrderId { get; protected set; }

        public string Name { get; protected set; }

        public decimal Quantity { get; protected set; }

        public string Unit { get; protected set; }

        protected OrderItem()
        {
            
        }

        public OrderItem(int orderId, string name, decimal quantity, string unit)
        {
            OrderId = orderId;
            Name = name;
            Quantity = quantity;
            Unit = unit;    
        }
    }
}