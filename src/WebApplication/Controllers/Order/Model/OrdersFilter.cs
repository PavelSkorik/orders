using System;

namespace WebApplication.Controllers.Order.Model
{
    public class OrdersFilter
    {
        public string OrderNumber { get; set; } = string.Empty;

        public DateTime? Date { get; set; }

        public int[] ProviderIds { get; set; } = new int[0];
        
        public string[] Units { get; set; } = new string[0];
        
        public string[] OrderItemNames { get; set; } = new string[0];
    }
}