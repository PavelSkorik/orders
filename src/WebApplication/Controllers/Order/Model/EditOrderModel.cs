using System.Collections.Generic;
using System.Linq;
using DomainModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication.Controllers.Order.Form;

namespace WebApplication.Controllers.Order.Model
{
    public class EditOrderModel
    {
        public readonly EditOrderForm Form;
        public readonly SelectList Providers;

        public EditOrderModel(DomainModel.Order order, IEnumerable<Provider> providers)
        {
            Providers = new SelectList(new HashSet<Provider>(providers, new ProviderComparer()), 
                "Id", "Name");
            Form = new EditOrderForm
            {
                Id = order.Id,
                Date = order.Date,
                Number = order.Number,
                ProviderId = order.ProviderId,
                Items = order.Items.Select(i => new Item
                {
                    Name = i.Name,
                    Quantity = i.Quantity,
                    Unit = i.Unit
                }).ToArray()
            };
        }
    }
}