using DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Provider> Providers { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }

        public static DbContextOptions<ApplicationDbContext> MakeOptions(
            IConfiguration configuration)
        {
            
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            return builder.Options;
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>(eb =>
            {
                eb.Property(o => o.Id).HasColumnType("int");
                eb.Property(o => o.Number).HasColumnType("nvarchar(max)");
                eb.Property(o => o.Date).HasColumnType("datetime2(7)");
                eb.Property(o => o.ProviderId).HasColumnType("int");
            });
            
            modelBuilder.Entity<OrderItem>(eb =>
            {
                eb.Property(o => o.Id).HasColumnType("int");
                eb.Property(o => o.OrderId).HasColumnType("int");
                eb.Property(o => o.Name).HasColumnType("nvarchar(max)");
                eb.Property(o => o.Quantity).HasColumnType("decimal(18, 3)");
                eb.Property(o => o.Unit).HasColumnType("nvarchar(max)");
            });
            
            modelBuilder.Entity<Provider>(eb =>
            {
                eb.Property(o => o.Id).HasColumnType("int");
                eb.Property(o => o.Name).HasColumnType("nvarchar(max)");
            });
        }
    }
}