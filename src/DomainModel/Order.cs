using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModel
{
    public class Order
    {
        public int Id { get; protected set; }
        public string Number { get; protected set; }
        public DateTime Date { get; protected set; }
        public int ProviderId { get; protected set; }

        [ForeignKey(nameof(ProviderId))]
        public Provider Provider { get; protected set; }
        
        public List<OrderItem> Items { get; protected set; } = new List<OrderItem>();

        protected Order()
        {
            
        }

        public Order(string number, DateTime date, int providerId)
        {
            if (string.IsNullOrEmpty(number))
            {
                throw new ArgumentNullException(nameof(number),"number is null");
            }
            
            Number = number;
            Date = date;
            ProviderId = providerId;
        }

        public void Update(string number, DateTime date, int providerId)
        {
            if (string.IsNullOrEmpty(number))
            {
                throw new ArgumentNullException(nameof(number),"number is null");
            }
            
            Number = number;
            Date = date;
            ProviderId = providerId;
        }

        public void AddItem(string name, decimal quantity, string unit)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name),"name is null");
            }
            
            if (string.IsNullOrEmpty(unit))
            {
                throw new ArgumentNullException(nameof(unit),"unit is null");
            }
            
            Items.Add(new OrderItem(Id, name, quantity, unit));
        }

        public void ClearItems() => Items.Clear();
    }
}
