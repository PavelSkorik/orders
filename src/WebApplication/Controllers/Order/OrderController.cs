using System;
using System.Linq;
using Infrastructure;
using Infrastructure.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Controllers.Order.Form;
using WebApplication.Controllers.Order.Handler;
using WebApplication.Controllers.Order.Model;

namespace WebApplication.Controllers.Order
{
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public OrderController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            var orders = _dbContext.Orders;
            var providers = _dbContext.Providers;
            var orderItems = _dbContext.OrderItems.ToList();
            var model = OrdersModel.Create(orders, orderItems, providers);
            return View(model);
        }
        
        [HttpPost]
        public IActionResult Index(OrdersFilter filter)
        {
            var orders = _dbContext.Orders.Include(a => a.Items);
            var providers = _dbContext.Providers;
            var orderItems = _dbContext.OrderItems.ToList();
            var model = OrdersModel.CreateWithFilter(orders,  providers, orderItems, filter);
            return View(model);
        }
        
        [HttpGet]
        public IActionResult View(int id)
        {
            var order = _dbContext.Orders
                .Include(a => a.Items)
                .Include(a => a.Provider)
                .SingleOrDefault(a => a.Id == id);
            
            if (order == null)
            {
                return Content($"order {id} not found");
            }
            
            return View(order);
        }
        
        [HttpPost]
        public IActionResult Delete(DeleteOrderForm form)
        {
            try
            {
                new DeleteOrderHandler(_dbContext).Execute(form);
                return RedirectToAction("Index");
            }
            catch (FormHandlerException e)
            {
                return Content(e.Message);
            }
        }
        
        [HttpGet]
        public IActionResult Create()
        { 
            var model = new CreateOrderModel(_dbContext.Providers);
            return View(model);
        }
        
        [HttpPost]
        public IActionResult Create(CreateOrderForm form)
        {
            try
            {
                new CreateOrderHandler(_dbContext).Execute(form);
                return RedirectToAction("Index");
            }
            catch (FormHandlerException e)
            {
                return Content(e.Message);
            }
        }
        
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var order = _dbContext.Orders
                .Include(a => a.Items)
                .Include(a => a.Provider)
                .SingleOrDefault(a => a.Id == id);
            
            if (order == null)
            {
                return Content($"order {id} not found");
            }
            
            var model = new EditOrderModel(order, _dbContext.Providers);
            return View(model);
        }
        
        [HttpPost]
        public IActionResult Edit(EditOrderForm form)
        {
            try
            {
                new EditOrderHandler(_dbContext).Execute(form);
                return RedirectToAction("Index");
            }
            catch (FormHandlerException e)
            {
                return Content(e.Message);
            }
        }
    }
}