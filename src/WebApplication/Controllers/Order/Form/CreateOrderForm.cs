using System;
using System.Collections.Generic;

namespace WebApplication.Controllers.Order.Form
{
    public class CreateOrderForm
    {
        public string Number { get; set; }
        public int ProviderId { get; set; }
        
        public Item[] Items { get; set; } = new Item[0];
    }
    
    public class Item
    {
        public string Name { get; set; }

        public decimal Quantity { get; set; }

        public string Unit { get; set; }
    }
}