using System;

namespace WebApplication.Controllers.Order.Form
{
    public class EditOrderForm : CreateOrderForm
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }
    }
}