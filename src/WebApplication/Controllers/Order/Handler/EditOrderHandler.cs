using System.Linq;
using Infrastructure;
using Infrastructure.DataAccess;
using Microsoft.EntityFrameworkCore;
using WebApplication.Controllers.Order.Form;

namespace WebApplication.Controllers.Order.Handler
{
    public class EditOrderHandler : FormHandlerBase<EditOrderForm>
    {
        public EditOrderHandler(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public override void Execute(EditOrderForm form)
        {
            var order = DbContext.Orders
                .Include(a => a.Items)
                .Include(a => a.Provider)
                .SingleOrDefault(a => a.Id == form.Id);
           
            if (order == null)
            {
                throw new FormHandlerException($"order {form.Id} not found");
            }

            order.Update(form.Number, form.Date, form.ProviderId);
            order.ClearItems();

            foreach (var item in form.Items)
            {
                order.AddItem(item.Name, item.Quantity, item.Unit);
            }

            DbContext.Orders.Update(order);
            DbContext.SaveChanges();
        }
    }
}