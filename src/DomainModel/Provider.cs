using System;
using System.Collections.Generic;

namespace DomainModel
{
    public class Provider 
    {
        public int Id { get; protected set; }

        public string Name { get; protected set; }

        protected Provider()
        {
            
        }
        public Provider(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name),"name is null");
            }
            
            Name = name;
        }
    }

    public class ProviderComparer : IEqualityComparer<Provider>
    {
        public bool Equals(Provider x, Provider y)
        {
            return x.Name == y.Name;
        }

        public int GetHashCode(Provider obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}