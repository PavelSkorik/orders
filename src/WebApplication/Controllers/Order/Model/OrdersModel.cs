using System.Collections.Generic;
using System.Linq;
using DomainModel;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Controllers.Order.Model
{
    public class OrdersModel
    {
        public IEnumerable<DomainModel.Order> Orders { get; }
        public SelectList Providers { get; }
        public OrdersFilter Filter { get; }
        public List<SelectListItem> OrderItemNames { get; }
        public List<SelectListItem> Units { get; }
        private OrdersModel(IEnumerable<DomainModel.Order> orders, 
            HashSet<string> units,
            HashSet<string> orderItemNames,
            HashSet<Provider> providers,
            OrdersFilter filter)
        {
            Orders = orders;
            OrderItemNames = orderItemNames
                .Distinct()
                .Select(name => new SelectListItem(name, name))
                .ToList();
            Units = units
                .Distinct()
                .Select(unit => new SelectListItem(unit, unit))
                .ToList();
            Providers = new SelectList(providers, "Id", "Name");
            Filter = filter;
        }
        
        public static OrdersModel Create(IEnumerable<DomainModel.Order> orders,
            List<OrderItem> orderItems,
            IEnumerable<Provider> providers)
        {
            return new OrdersModel(orders: orders,
                units: new HashSet<string>(orderItems.Select(a => a.Unit)),
                orderItemNames: new HashSet<string>(orderItems.Select(a => a.Name)),
                providers: new HashSet<Provider>(providers, new ProviderComparer()),
                new OrdersFilter());
        }
        public static OrdersModel CreateWithFilter(IQueryable<DomainModel.Order> orders,
            IQueryable<Provider> providers,
            List<OrderItem> orderItems,
            OrdersFilter filter)
        {
            IEnumerable<DomainModel.Order> filteredOrders = orders;
            if (filter.Date.HasValue)
            {
                filteredOrders = filteredOrders.Where(a => a.Date.Date == filter.Date.Value.Date);
            }

            if (filter.ProviderIds.Any())
            {
                filteredOrders = filteredOrders.Where(a => filter.ProviderIds.Contains(a.ProviderId));
            }

            if (!string.IsNullOrEmpty(filter.OrderNumber))
            {
                filteredOrders = filteredOrders.Where(a => a.Number == filter.OrderNumber);
            }
            
            if (filter.OrderItemNames.Any())
            {
                var possibleOrders = orderItems
                    .Where(a => filter.OrderItemNames.Contains(a.Name))
                    .Select(a => a.OrderId)
                    .ToHashSet();

                filteredOrders = filteredOrders.Where(a => possibleOrders.Contains(a.Id));
            }
            
            if (filter.Units.Any())
            {
                var possibleOrders = orderItems
                    .Where(a => filter.Units.Contains(a.Unit))
                    .Select(a => a.OrderId)
                    .ToHashSet();
                
                filteredOrders = filteredOrders.Where(a => possibleOrders.Contains(a.Id));
            }
            
            return new OrdersModel(orders: filteredOrders.ToList(),
                units: new HashSet<string>(orderItems.Select(a => a.Unit)),
                orderItemNames: new HashSet<string>(orderItems.Select(a => a.Name)),
                providers: new HashSet<Provider>(providers, new ProviderComparer()),
                filter: filter);
        }
        
    }
}