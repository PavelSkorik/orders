using System;
using Infrastructure;
using Infrastructure.DataAccess;
using WebApplication.Controllers.Order.Form;

namespace WebApplication.Controllers.Order.Handler
{
    public class CreateOrderHandler : FormHandlerBase<CreateOrderForm>
    {
        public CreateOrderHandler(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public override void Execute(CreateOrderForm form)
        {
            var order = new DomainModel.Order(form.Number, DateTime.Now, form.ProviderId);
            foreach (var item in form.Items)
            {
                order.AddItem(item.Name, item.Quantity, item.Unit);
            }

            DbContext.Orders.Add(order);
            DbContext.SaveChanges();
        }
    }
}