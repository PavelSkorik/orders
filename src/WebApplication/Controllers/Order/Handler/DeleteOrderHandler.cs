using System;
using System.Linq;
using Infrastructure;
using Infrastructure.DataAccess;
using Microsoft.EntityFrameworkCore;
using WebApplication.Controllers.Order.Form;

namespace WebApplication.Controllers.Order.Handler
{
    public class DeleteOrderHandler : FormHandlerBase<DeleteOrderForm>
    {
        public DeleteOrderHandler(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public override void Execute(DeleteOrderForm form)
        {
            var order = DbContext.Orders
                .Include(a => a.Items)
                .SingleOrDefault(a => a.Id == form.Id);
            
            if (order == null)
            {
                throw new FormHandlerException($"order {form.Id} not found");
            }

            DbContext.Orders.Remove(order);
            DbContext.SaveChanges();
        }
    }
}