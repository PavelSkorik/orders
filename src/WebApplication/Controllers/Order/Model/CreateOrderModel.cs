using System.Collections.Generic;
using DomainModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication.Controllers.Order.Form;

namespace WebApplication.Controllers.Order.Model
{
    public class CreateOrderModel
    {
        public readonly CreateOrderForm Form;
        public readonly SelectList Providers;

        public CreateOrderModel(IEnumerable<Provider> providers)
        {
            Providers = new SelectList(new HashSet<Provider>(providers, new ProviderComparer()), 
                "Id", "Name");
            Form = new CreateOrderForm();
        }
    }
}