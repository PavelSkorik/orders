using System;
using Infrastructure.DataAccess;

namespace Infrastructure
{
    public abstract class FormHandlerBase<TForm> 
    {
        protected readonly ApplicationDbContext DbContext;

        protected FormHandlerBase(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public abstract void Execute(TForm form);
    }

    public class FormHandlerException : ApplicationException
    {
        public FormHandlerException(string message) : base(message)
        {
        }

        public FormHandlerException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}